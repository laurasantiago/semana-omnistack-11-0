import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// Renderizando App dentro do ID root
ReactDOM.render(<App />, document.getElementById('root') );
