const express = require('express'); // importando o módulo, todas as funcionalidades agr estão na var
const routes = require('./routes');
const cors = require('cors');

const app = express(); // app vai armazenar a aplicação

app.use(cors()); // permite que o back seja acessado pelo front
app.use(express.json()); // avisar pro express p usar body em JSON
app.use(routes);

app.listen(3333); // porta do app