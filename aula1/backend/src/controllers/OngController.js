const connection = require('../database/connection');
const crypto = require('crypto');

module.exports = {
  async create(request, response) {
    const {name, email, whatsapp, city, uf} = request.body; // body é o json que eu to usando pra passar os valores do objeto criado

    const id = crypto.randomBytes(4).toString('HEX'); // gera string aleatoria e muda de byte pra hex
    await connection('ongs').insert({
      id,
      name,
      email,
      whatsapp,
      city,
      uf
    });

    return response.json({ id });
  },
  async index (request, response) {
    // ongs = conectar a tabela onts e selecionar todos seus campos
    const ongs = await connection('ongs').select('*');
    return response.json(ongs);
  }
}