const connection = require('../database/connection');

module.exports = {
  async create(request, response) {
    const { title, description, value } = request.body;
    const ong_id = request.headers.authorization;

    const [id] = await connection('incidents').insert({
      title,
      description,
      value,
      ong_id
    });

    return response.json({ id });
  },
  async index(request, response) {
    const [count] = await connection('incidents')
      .count(); // [] pra pegar primeiro elem da array

    const { page = 1 } = request.query; // 1 é o valor padrão caso n venha no request

    // offset é a quantidade a pular
    // na página 1, pula 0 registros
    // na página 2, pula 5 registros (5 pag. 1)
    // na página 3, pula 10 geristros (5 pag. 1 e 5 pag. 2)

    // join ongs (pega da tabela 'ongs'), objetos em que 'ongs.id' são iguais a incidents.ong_id
    // incidents.* é todos os dados do incidente
    // ongs.X é o parâmetro X da ONG
    const incidents = await connection('incidents')
      .join('ongs', 'ongs.id', '=', 'incidents.ong_id')
      .limit(5)
      .offset(( page - 1 ) * 5)
      .select([
        'incidents.*',
        'ongs.name',
        'ongs.email',
        'ongs.whatsapp',
        'ongs.city',
        'ongs.uf'
      ]);
    
    response.header('X-Total-Count', count['count(*)']) // a header de nome X-Total-Count terá o valor count['count(*)']
    return response.json(incidents);
  },
  async delete(request, response) {
    const { id } = request.params;
    const ong_id = request.headers.authorization; // verifica se o incidente criado pertence à ong que pediu pra deletar ele
    const incident = await connection('incidents')
      .where('id', id)
      .select('ong_id')
      .first();
    if (incident.ong_id != ong_id) {
      return response.status(401).json({ error: 'Operation not commited' }); // unauthorized
    }
    await connection('incidents').where('id', id).delete();

    return response.status(204).send();
  }
}