
exports.up = function(knex) {
  return knex.schema.createTable('incidents', function (table) {
    table.increments(); // chave primária que incrementa sozinho (1, 2, 3, 4...)

    table.string('title').notNullable(); // não pode ser nulo
    table.string('description').notNullable();
    table.decimal('value').notNullable();
    
    table.string('ong_id').notNullable();

    // ong_id referencia id na tabela ongs
    table.foreign('ong_id').references('id').inTable('ongs');
  })
};

exports.down = function(knex) { // e se der algum problema?
  return knex.schema.dropTable('incidents');
};
