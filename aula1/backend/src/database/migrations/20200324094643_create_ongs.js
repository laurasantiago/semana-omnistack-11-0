
exports.up = function(knex) {
  return knex.schema.createTable('ongs', function (table) {
    table.string('id').primary(); // chave primária
    table.string('name').notNullable(); // não pode ser nulo
    table.string('email').notNullable();
    table.string('whatsapp').notNullable();
    table.string('city').notNullable();
    table.string('uf', 2).notNullable(); // tamanho máx do texto
  })
};

exports.down = function(knex) { // e se der algum problema?
  return knex.schema.dropTable('ongs');
};
