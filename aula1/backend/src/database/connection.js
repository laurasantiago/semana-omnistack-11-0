const knex = require('knex');
const configuration = require('../../knexfile'); // config do bd

const connection = knex(configuration.development); // config de desenvolvimento

module.exports = connection;